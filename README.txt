=======================
Amazon S3 Logs
=======================
Richard Peacock (richard@richardpeacock.com)

This module reads and parses your Amazon S3 account's various
log files, so you can easily track your most popular uri's,
and how much bandwidth has been used per-file.  It also makes sure
to never download the same log file twice (saving you bandwidth),
and can be configured to only check for new log files every few hours
(saving you GET and LIST requests).

Requires: the Amazon S3 module (http://drupal.org/project/amazon_s3)
Also requires that you have set up a special log bucket as per
Amazon's instructions: http://docs.amazonwebservices.com/AmazonS3/2006-03-01/LoggingHowTo.html
I know that may seem hard or anoying, but it honestly only takes about
five minutes.

Special note: If any developers want to work on a way to automate the
process of creating a log bucket, I would greatly appreciate it!  Please
post your thoughts or code in an issue on http://drupal.org/project/amazon_s3_logs.

===============
Directions
===============
- Install the required amazon_s3 module, and set up a special log bucket
  on your Amazon S3 account (follow the directions in the link above).
- Install this module at /modules/amazon_s3_logs and enable it.
- Visit admin/settings/amazon-s3-logs to configure.
- Wait for log files to appear in your log bucket, and make sure your cron
  is running correctly.
- View your usage statistics at: Administer -> Reports -> Amazon S3 Logs
  or at admin/reports/amazon-s3-logs